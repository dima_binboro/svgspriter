var fs = require('fs');
var path = require('path');


module.exports = {
      
    svg_files       : {},
    symbols         : [],
    
    current_file    : 0,
    
    source_folder   : '',
    target_file     : '',
    preview_page    : false,
    
    str_to_save : '',
    
    run : function(source_folder, target_file, preview_page = false) {
    
        var self = this;
        
        self.preview_page    = preview_page;
    
        self.source_folder   = source_folder;
        self.target_file     = target_file;
    
    
        fs.readdir(source_folder, function(err, svg_files) {
         
            self.svg_files = svg_files;
            
            self.read_files();
              
        });
    },
  
  
  
    read_files : function() {
      
        var self = this;
        
        /*
            All file read
            Save all SVG to one file
        */
        if (!self.svg_files[this.current_file]) {
            
            console.log("No more files, "  + this.current_file);
            self.write_file();
            
            if (self.preview_page !== false) {
                
                var file = path.join(__dirname, '', 'preview_style.css');
                
                fs.readFile(file, 'utf8', function(err, data) {
                    
                    self.style = data;
                    
                    self.make_preview_page();
                });
            }
                
            
            return true;
        }
        
        
        var file = self.svg_files[this.current_file];
        
        fs.readFile(this.source_folder + "/" + file, 'utf8', function(err, data) {
           
            self.process_file(data);

          
            self.current_file++;
            
            self.read_files();
            
        });
            
    
      
      
    },
    
    
    process_file : function(content) {
        
        var cleaned_svg =  this.clean_svg(content);
        var symbol_id   =  this.file_to_symbol_id();
        var view_box    =  this.get_view_box(content);
 
        var str = "\n";
            str += "\t"+'<symbol id="'+symbol_id+'"> <!--' + view_box + ' --> ';
            str += "\n";
        
                str += "\t\t"+cleaned_svg
        
            str += "\n";
            str += "\t"+"</symbol>";
            str += "\n\n";
        
       
        this.symbols.push({
           view_box : view_box,
           id       : symbol_id,
           svg      : cleaned_svg,
           symbol   : str,
        });
        
        
        this.str_to_save += str;
    },
    
    get_view_box: function(content) {
        
        var viewBox = '';
        var matches = content.match(/viewBox\=\"0 0 (.*?)\"/);
        if (matches && matches[1]) {
            viewBox = matches[1];
        }
        
        return viewBox;
    },
    file_to_symbol_id : function() {
        
        var file = this.svg_files[this.current_file];
        
        file = file.replace(/\s+/g, "_");
        return  file.replace(/.svg/g, "");
        
    },
    /*
        Removal of unneeded attributes and tags
    */
    clean_svg : function (content) {
        
        if (!content)
            return '';
        
        var cleaned = '';
       
       
        content = content.replace(/\r?\n/g, "");
        content = content.replace(/\t/g, "");
       
        var matches = content.match(/\<svg(.*?)\>/); 
        if (matches && matches[0])       
            cleaned = content.replace(matches[0], '<svg>');
        
        
        if (cleaned == '') {
            return '';
        }

       

        
        var cleaned = cleaned.match(/\<svg\>(.*?)\<\/svg\>/);       
        if (matches && matches[1])       
            cleaned = cleaned[1];
       
       
        
        var matches = cleaned.match(/fill\:(.*?)\;/);
        if (matches && matches[0])  
            cleaned = cleaned.replace(matches[0], '');


        var matches = cleaned.match(/stroke\:(.*?)\;/);
        if (matches && matches[0])  
            cleaned = cleaned.replace(matches[0], '');
        
        
        var matches = cleaned.match(/fill\=\"(.*?)\"/);
        if (matches && matches[0])  
            cleaned = cleaned.replace(matches[0], '');


        var matches = cleaned.match(/stroke\=\"(.*?)\"/);
        if (matches && matches[0])  
            cleaned = cleaned.replace(matches[0], '');     
        
        
        var matches = cleaned.match(/class\=\"(.*?)\"/);
        if (matches && matches[0])  
            cleaned = cleaned.replace(matches[0], '');          

        cleaned = cleaned.replace(/\r?\n/g, "");        
         
         
        return cleaned;
        
    },
  
  
    write_file : function() {
        var self = this;
      
      
        self.str_to_save = '<div class="svg_all">' + "\n" + '<svg xmlns="http://www.w3.org/2000/svg">'+ "\n\n" + self.str_to_save;
        
        self.str_to_save += "" + '</svg>\n</div>';
         

      
        fs.writeFile(self.target_file, self.str_to_save, function(err) {
            if(err) {
                return console.log(err);
            }

            console.log("The file was saved!");
        }); 
    },
    
    
    make_preview_page : function() {
        var self = this;
        
        
        var style = '<style>' + this.style + '</style>';
                

            
            
        var grid = '<div class="grid">';
      
        for ( var i in this.symbols) {
            
            var s = this.symbols[i];
            
            grid += '<div class="grid__item">';
                grid += '<div class="grid__icon"><svg viewBox="0 0 '+s.view_box+'"><use xlink:href="#'+s.id+'"></use></svg></div>';
                grid += '<div class="grid__popup">';
                grid += '<div class="grid__icon"><svg viewBox="0 0 '+s.view_box+'"><use xlink:href="#'+s.id+'"></use></svg></div>';
                grid += '<div class="grid__html"><h2>HTML:</h2><textarea><svg viewBox="0 0 '+s.view_box+'"><use xlink:href="#'+s.id+'"></use></svg></textarea></div>';
                grid += '<div class="grid__jade"><h2>Jade:</h2><textarea>svg.bank__soc__logo(viewBox="0 0 '+s.view_box+'")';
                grid += "\n\t" +'use(xlink:href="#'+s.id+'")</textarea></div>';
                grid += '<div class="grid__symbol"><h2>Symbol:</h2><textarea>'+s.symbol+'</textarea></div>';
                grid += '</div>';
                
            grid += '</div>';
            
        }
        
        grid += '</div>';
        
        
        var html = "<html>\n\t<head>"+style+"</head>\n<body>" + this.str_to_save + grid + "\n<body>\n</html>";
        
        
        fs.writeFile(self.preview_page, html, function(err) {
            if(err) {
                return console.log(err);
            }

            console.log("Preview is done");
        }); 
        
    }
    
};