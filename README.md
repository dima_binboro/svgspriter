Svg Spriter
=========


## Overal
Script generates SVG sprite from SVG files with nice looking preview. Created for fast slicing and very simple configuration. 

## Functions

- Take all svg files from specified folder and creates one SVG with symbols (svg sprite). It saves it to svg.html or any other file you specify. 
- Generates usefull svg_preview.html where you can pick icons easily during slice instead of checking your sprite.
- For gulp or standalone usage.
- Very simple installation and usage.
- Simple and obvious source code for changes.



Has preview!


## Video

- Please watch video to see it in action

https://youtu.be/SmmT5ClG8-c

## Installation

  npm install svgspriter --save

## Usage

var gulp = require('gulp'),
    svgspriter = require('svgspriter');


gulp.task('svg', function() {
    svgspriter.run("svg_files", "svg.html", "svg_preview.html");
});


## Release History
* 1.0.0 Initial release

* 1.0.6 Bug fixes

## Author

Dmitro Shatalov
dimashatalov@gmail.com
